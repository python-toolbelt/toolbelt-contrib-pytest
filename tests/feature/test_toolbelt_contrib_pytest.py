import pytest

# TODO: just test the presence of utils for now

def test_not_implemented_feature_mark():
  from toolbelt.contrib.pytest import not_implemented_feature_mark

def test_not_implemented_feature():
  from toolbelt.contrib.pytest import not_implemented_feature

def test_known_bug():
  from toolbelt.contrib.pytest import known_bug
