# Changelog

Tracking changes in `toolbelt-contrib-pytest` between versions. For a complete view of all the releases, visit GitLab:

https://gitlab.com/python-toolbelt/toolbelt-contrib-pytest/-/releases

## next release
- added `not_implemented_feature_mark`
- added `not_implemented_feature` mark shortcut
- added `known_bug` mark
