import pytest

def not_implemented_feature_mark(**kwargs):
  return pytest.mark.skip(reason = 'Not implemented feature', **kwargs)

def not_implemented_feature(test_func):
  return not_implemented_feature_mark(test_func)

def known_bug(issue_url):
  def _known_bug(test_func):
    reason = 'Regression test for known bug.'

    if issue_url: reason += f'\nSee {issue_url} for more informations.'

    return pytest.mark.skip(reason = reason)(test_func)

  if callable(issue_url):
    test_func, issue_url = issue_url, ''

    return _known_bug(test_func)

  return _known_bug
